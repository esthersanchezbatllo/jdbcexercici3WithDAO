/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.copernic.jdbcexercici3;

import cat.copernic.jdbcexercici3.dao.EmpleadoDao;
import cat.copernic.jdbcexercici3.dao.EmpleadoDaoImpl;
import cat.copernic.jdbcexercici3.model.Empleado;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;

/**
 *
 * @author EstherSanchez
 */
public class Menu {

    private Scanner scanner;
    private EmpleadoDao dao;

    public Menu() { // constructor
        scanner = new Scanner(System.in);
        dao = EmpleadoDaoImpl.getInstance();
    }

    public void init() {
        String opcioStr;
        int opcio= 0;
        try {
            do {
                menu();
                opcioStr = scanner.nextLine();
                if (isInteger(opcioStr)) {
                    opcio = Integer.valueOf(opcioStr);
                    switch (opcio) {
                        case 1:
                            listAll();
                            break;
                        case 2:
                            listById();
                            break;
                        case 3:
                            insert();
                            break;
                        case 4:
                            update();
                            break;
                        case 5:
                            delete();
                            break;
                        case 0:
                            System.out.println("Sortint del programa... \n");
                            break;
                        default:
                            System.err.println("El número que has introduït no es correspon amb una operació vàlida\n");
                    }
                } else {
                    System.out.println("No has introduït un número vàlid");
                }

            } while (opcio != 0);
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }

    public static boolean isInteger(String text) {
        int v;
        try {
            v = Integer.parseInt(text);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    public void menu() {
        System.out.println("SISTEMA DE GESTIÓ D'EMPLEATS");
        System.out.println("============================");
        System.out.println("-> Introdueix una opció entre les següents:\n");
        System.out.println("0: Sortir");
        System.out.println("1: Llistar tots els empleats");
        System.out.println("2: Llistar un empleat pel seu ID");
        System.out.println("3: Insertar un nou empleat");
        System.out.println("4: Actualitzar un empleat");
        System.out.println("5: Eliminar un empleat");
        System.out.print("\nOpció:");

    }

    public void insert() {
        System.out.println("INSERCIÓ D'UN NOU EMPLEAT");
        System.out.println("=========================");
        System.out.println("-> Introdueix el nom (sense cognoms) de l'empleat: ");
        String nombre = scanner.nextLine();
        System.out.println("-> Introdueix els cognoms de l'empreat: ");
        String apellidos = scanner.nextLine();
        System.out.println("-> Introdueix la data de naixement de l'empleat (dd/MM/yyyy): ");
        String fechaNacimientoStr = scanner.nextLine(); //es podria millorar amb un bucle fins que s'entri en el format adequat.
        LocalDate fechaNacimiento = LocalDate.parse(fechaNacimientoStr, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        System.out.println("-> Introdueix el lloc de treball de l'empleat: ");
        String puesto = scanner.nextLine();
        System.out.println("-> Introdueix el email de l'empleat: ");
        String email = scanner.nextLine();
        try {
            dao.add(new Empleado(nombre, apellidos, fechaNacimiento, puesto, email));
            System.out.println("Nou empleat registrat");
        } catch (SQLException ex) {
            System.err.println("Error al insertar un nou registre en la base de dades.");
        }
        System.out.println("");

    }

    private void listAll() {
        System.out.println("\n LLISTAT DE TOTS ELS EMPLEATS");
        System.out.println("-------------------------------\n");

        try {
            List<Empleado> result = dao.getAll();
            if (result.isEmpty()) {
                System.out.println("No hi ha empleats registrats en la base de dades");
            } else {
                printCabeceraTablaEmpleado();
                result.forEach(this::printEmpleado);
            }
        } catch (SQLException e) {
            System.err.println("Error consultant a la base de dades.");

        }
        System.out.println("\n");

    }

    private void printCabeceraTablaEmpleado() {
        System.out.printf("%2s %25s %8s %25s %25s", "ID", "NOM", "DATA NAIX.", "LLOC", "EMAIL");
        System.out.println("");
        IntStream.range(1, 100).forEach(x -> System.out.print("-")); //imprimim 100 guions
    }

    private void printEmpleado(Empleado emp) {
        System.out.printf("\n%2s %25s %8s %25s %25s",
                emp.getId_empleado(),
                emp.getNombre() + " " + emp.getApellidos(),
                emp.getFechaNacimiento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
                emp.getPuesto(),
                emp.getEmail());
    }

    private void listById() {
        System.out.println("\nCERCA D'EMPLEATS PER ID");
        System.out.println("-------------------------------\n");
        try {
            System.out.print("\nIntrodueix el ID del empleat a buscar: ");
//            int id = reader.nextInt();

            String entradaStr = scanner.nextLine();
            if (isInteger(entradaStr)) {
                int id = Integer.valueOf(entradaStr);

                Empleado empleado = dao.getById(id);
                if (empleado == null) {
                    System.out.println("No hi ha cap resultat amb aquest id");
                } else {
                    printCabeceraTablaEmpleado();
                    printEmpleado(empleado);
                }
                System.out.println("\n");
            } else {
                System.out.println("No has introduït un id vàlid");

            }
        } catch (SQLException ex) {
            System.err.println("Error consultant a la base de dades per id: " + ex.getMessage());
        }

    }

    private void update() {
        System.out.println("\nACTUALTIZACIÓ D'UN EMPLEAT");
        System.out.println("----------------------------\n");
        try {
            System.out.print("Introdueix el ID de l'empleat a buscar: ");
            String entradaStr = scanner.nextLine();
            if (isInteger(entradaStr)) {
                int id = Integer.valueOf(entradaStr);

                Empleado empleado = dao.getById(id);
                if (empleado == null) {
                    System.out.println("No hi ha empleats a la base de dades amb aquest id.");
                } else {
                    printCabeceraTablaEmpleado();
                    printEmpleado(empleado);
                    System.out.println("\n");

                    System.out.printf("-> Introdueix el nom (sense cognoms) de l'empleat (%s): ", empleado.getNombre());
                    String nombre = scanner.nextLine();
                    nombre = (nombre.isBlank()) ? empleado.getNombre() : nombre; //si no hem escrit res vol dir que volem el mateix nom.

                    System.out.printf("-> Introdueix els cognoms de l'empleat (%s): ", empleado.getApellidos());
                    String apellidos = scanner.nextLine();
                    apellidos = (apellidos.isBlank()) ? empleado.getApellidos() : apellidos;

                    System.out.printf("-> Introdueix la data de naixement de l'empleat (format dd/MM/aaa) (%s):  ",
                            empleado.getFechaNacimiento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                    String strFechaNacimiento = scanner.nextLine();
                    LocalDate fechaNacimiento = (strFechaNacimiento.isBlank()) ? empleado.getFechaNacimiento()
                            : LocalDate.parse(strFechaNacimiento, DateTimeFormatter.ofPattern("dd/MM/yyyy"));

                    System.out.printf("-> Introdueix el lloc de treball de l'empleat (%s): ", empleado.getPuesto());
                    String puesto = scanner.nextLine();

                    puesto = (puesto.isBlank()) ? empleado.getPuesto() : puesto;

                    System.out.printf("-> Introdueix el email de l'empleat (%s): ", empleado.getEmail());
                    String email = scanner.nextLine();
                    email = (email.isBlank()) ? empleado.getEmail() : email;

                    empleado.setNombre(nombre);
                    empleado.setApellidos(apellidos);
                    empleado.setFechaNacimiento(fechaNacimiento);
                    empleado.setPuesto(puesto);
                    empleado.setEmail(email);

                    dao.update(empleado);

                    System.out.println("");
                    System.out.printf("Empleat amb ID %s actualitzat", empleado.getId_empleado());
                    System.out.println("");

                }
                System.out.println("\n");

            } else {
                System.out.println("No has introduït un id vàlid");

            }

        } catch (SQLException ex) {
            System.err.println("Error actualitzant la base de dades");
        }
    }

    private void delete() {
        System.out.println("\nBORRAT D'UN EMPLEAT");
        System.out.println("----------------------");

        try {
            System.out.print("Introdueix el ID de l'empleat a esborrar: ");
            String entradaStr = scanner.nextLine();
            if (isInteger(entradaStr)) {
                int id = Integer.valueOf(entradaStr);

                System.out.printf("Estàs segur que vols eliminar l'empleat amb ID=%s? (s/n): ", id);
                String borrar = scanner.nextLine();

                if (borrar.equalsIgnoreCase("s")) {
                    int x = dao.delete(id);
                    if (x == 1) {
                        System.out.printf("L'empleat amb id %s ha estat eliminat.\n", id);
                    } else {
                        System.out.println("No s'ha trobat el registre.\n");
                    }
                }
            } else {
                System.out.println("No has introduït un id vàlid");

            }
        } catch (SQLException ex) {

        }
    }


}
