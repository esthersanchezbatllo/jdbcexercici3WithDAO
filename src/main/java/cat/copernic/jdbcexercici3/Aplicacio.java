/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package cat.copernic.jdbcexercici3;

import cat.copernic.jdbcexercici3.dao.EmpleadoDao;
import cat.copernic.jdbcexercici3.dao.EmpleadoDaoImpl;
import cat.copernic.jdbcexercici3.model.Empleado;
import cat.copernic.jdbcexercici3.pool.MyDataSource;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author EstherSanchez
 */
public class Aplicacio {

    public static void main(String[] args) {
    
        Menu menu = new Menu();
        menu.init();
    }
}
