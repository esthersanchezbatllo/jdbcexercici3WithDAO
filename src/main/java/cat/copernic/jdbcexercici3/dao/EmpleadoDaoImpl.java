/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.copernic.jdbcexercici3.dao;

import cat.copernic.jdbcexercici3.model.Empleado;
import cat.copernic.jdbcexercici3.pool.MyDataSource;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author EstherSanchez
 */
public class EmpleadoDaoImpl implements EmpleadoDao {

    //Declararem una variable estàtica perquè poder implementar un patró singleton  
    private static EmpleadoDaoImpl instance;

    static { //instanciem l'únic objecte d'aquesta classe
        instance = new EmpleadoDaoImpl();
    }

    private EmpleadoDaoImpl() {
    } //el declarem privat perquè ningú el pugui cridar

    public static EmpleadoDaoImpl getInstance() {
        return instance;
    }

    //ens obliga implementar tots els mètodes de la interfaç EmpleadoDao que implementem
    @Override
    public int add(Empleado emp) throws SQLException {
        String sql = "INSERT INTO empleado (nombre, apellidos, fecha_nacimiento, puesto, email) VALUES (?,?,?,?,?);";
        int result;
        try ( Connection conn = MyDataSource.getConnection();  PreparedStatement pstm = conn.prepareStatement(sql)) {
            setejarPreparedStatement(pstm, emp);
            result = pstm.executeUpdate();
            // no ens fa falta un catch perquè estem dient que fem un throws i el mètode que el cridi add és el que tractarà l'excepció.
            return result;
        }
    }

   

    @Override
    public Empleado getById(int id) throws SQLException {
        Empleado result = null;
        String sql = "SELECT * FROM empleado WHERE id_empleado = ?";

        try ( Connection conn = MyDataSource.getConnection();  PreparedStatement pstm = conn.prepareStatement(sql)) {

            pstm.setInt(1, id);

            // necessitarem un altre try perquè necessitavem passar-li el paràmetre i no ho podiem fer dins del resource del primer try.
            try ( ResultSet rs = pstm.executeQuery()) {
                while (rs.next()) {
                    result = obtainEmpFromRs(rs);
                }
            }
        }
        return result;
    }

    @Override
    public List<Empleado> getAll() throws SQLException {
        String sql = "SELECT * FROM empleado";

        List<Empleado> result = new ArrayList<>();

        try ( Connection conn = MyDataSource.getConnection();  PreparedStatement pstm = conn.prepareStatement(sql);  ResultSet rs = pstm.executeQuery()) {
            Empleado emp;
            while (rs.next()) {
                emp = obtainEmpFromRs(rs);
                result.add(emp);
            }

        }

        return result;
    }

    @Override
    public int update(Empleado emp) throws SQLException {
        String sql = "UPDATE empleado SET nombre =?, apellidos=?, fecha_nacimiento=?, puesto=?, email=? WHERE id_empleado =?";

        int result;

        try ( Connection conn = MyDataSource.getConnection();  PreparedStatement pstm = conn.prepareStatement(sql)) {

            setejarPreparedStatement(pstm, emp);
            pstm.setInt(6, emp.getId_empleado());
            
            result = pstm.executeUpdate();

        }

        return result;
    }

    @Override
    public int delete(int id) throws SQLException {
        String sql = "DELETE FROM empleado WHERE id_empleado=?";
         try ( Connection conn = MyDataSource.getConnection();  PreparedStatement pstm = conn.prepareStatement(sql)) {

            pstm.setInt(1, id);
            
            return pstm.executeUpdate();
         }
        
    }
    
     private void setejarPreparedStatement(final PreparedStatement pstm, Empleado emp) throws SQLException {
        pstm.setString(1, emp.getNombre());
        pstm.setString(2, emp.getApellidos());
        pstm.setDate(3, Date.valueOf(emp.getFechaNacimiento()));
        pstm.setString(4, emp.getPuesto());
        pstm.setString(5, emp.getEmail());
    }

    private Empleado obtainEmpFromRs(final ResultSet rs) throws SQLException {
        Empleado emp;
        emp = new Empleado();
        emp.setId_empleado(rs.getInt("id_empleado"));
        emp.setNombre(rs.getString("nombre"));
        emp.setApellidos(rs.getString("apellidos"));
        emp.setFechaNacimiento(rs.getDate("fecha_nacimiento").toLocalDate());
        emp.setPuesto(rs.getString("puesto"));
        emp.setEmail(rs.getString("email"));
        return emp;
    }

}
