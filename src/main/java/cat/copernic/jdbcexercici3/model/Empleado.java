/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.copernic.jdbcexercici3.model;

import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author EstherSanchez
 */
public class Empleado {

    private int id_empleado;
    private String nombre;
    private String apellidos;
    private LocalDate fechaNacimiento;
    private String puesto;
    private String email;
    
    public Empleado() {
    }
    public Empleado(String nombre, String apellidos, LocalDate fechaNacimiento, String puesto, String email) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.fechaNacimiento = fechaNacimiento;
        this.puesto = puesto;
        this.email = email;
    }

    public Empleado(int id_empleado, String nombre, String apellidos, LocalDate fechaNacimiento, String puesto, String email) {
        this(nombre, apellidos, fechaNacimiento, puesto, email);
        this.id_empleado = id_empleado;
    }

   
    public void setId_empleado(int id_empleado) {
        this.id_empleado = id_empleado;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId_empleado() {
        return id_empleado;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public String getPuesto() {
        return puesto;
    }

    public String getEmail() {
        return email;
    }
    
    @Override
    public int hashCode() {
        return this.id_empleado;
    }
    
    /*
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + this.id_empleado;
        hash = 59 * hash + Objects.hashCode(this.nombre);
        hash = 59 * hash + Objects.hashCode(this.apellidos);
        hash = 59 * hash + Objects.hashCode(this.fechaNacimiento);
        hash = 59 * hash + Objects.hashCode(this.puesto);
        hash = 59 * hash + Objects.hashCode(this.email);
        return hash;
    }
*/
   
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Empleado other = (Empleado) obj;
        return (this.id_empleado == other.id_empleado);
    }

    @Override
    public String toString() {
        return "Empleado{" + "id_empleado=" + id_empleado +
                ", nombre=" + nombre +
                ", apellidos=" + apellidos +
                ", fechaNacimiento=" + fechaNacimiento +
                ", puesto=" + puesto +
                ", email=" + email + '}';
    }
    
    
}
